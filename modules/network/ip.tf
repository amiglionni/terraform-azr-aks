resource "random_string" "fqdn" {
  length  = 6
  special = false
  upper   = false
  number  = false
}

resource "azurerm_public_ip" "k8s" {
  name                = "public-ip"
  location            = var.location
  resource_group_name = azurerm_resource_group.k8s.name
  allocation_method   = "Static"
  domain_name_label   = random_string.fqdn.result
  tags                = var.tags
}