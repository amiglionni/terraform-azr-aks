variable "subscription_id" {
  type    = "string"
  description = "Assinatura na Azure onde nossos recursos estarao."
  default = ""
}

variable "security_group_name" {
  type    = "string"
  description = "Grupo de Seguranca para permitir acessos."
  default = "sg-aks-default"
}

variable "virtual_network" {
  type    = "string"
  description = "Rede virtual na Azure."
  default = "vn-aks-eastus2"
}

variable "resource_group" {
  type    = "string"
  description = "Grupo de Recursos para agrupar os servicos criados."
  default = "rg-aks-teste"
}

variable "location" {
  type    = "string"
  description = "Regiao da Azure onde os recursos serao criados."
  default = "East US 2"
}

variable "location2" {
  type    = "string"
  description = "Regiao da Azure onde os recursos serao criados."
  default = "East US"
}

variable "vm_worker01" {
  type    = string
  default = "worker01"
}

variable "vm_worker02" {
  type    = string
  default = "worker02"
}

variable "vm_k8s_master" {
  type    = string
  default = "k8s-master"
}

variable "admin_user" {
    default = "adminuser"
}

variable "admin_password" {
    default = "Password%1234"
}

variable "size_vm" {
    description = "Tamanho de Instancia Virtual."
    default = "Standard_B2ms"
}

variable "tags" { 
    type = "map" 
    default = { 
        Name: "value",
        Created: "Terraform",
        Owner: "Alberto Miglionni",
	Cluster: "AKS",
        Env: "Developer"
  } 
}
