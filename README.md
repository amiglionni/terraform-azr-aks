Por Alberto Miglionni

Implantar um cluster do AKS (Serviço de Kubernetes do Azure) usando código Terraform.
O AKS (Serviço de Kubernetes do Azure) é um serviço de Kubernetes gerenciado que permite implantar e gerenciar clusters rapidamente. 
Neste repositório, implante um cluster AKS usando o Terraform.
